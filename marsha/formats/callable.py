import typing
from .any import Any
from ..format_registry import FormatRegistry


class Callable(Any):
    model_type = typing.Callable
    dump_format = Any

    @classmethod
    def from_type(self, dump_type):
        dump_type = dump_type.__args__[1]

        class SpecificCallable(self):
            dump_format = FormatRegistry.get(dump_type)

        return SpecificCallable

    @classmethod
    def dump(self, fn):
        return self.dump_format.dump(fn())
