from .raw_format import RawFormat


class Float(RawFormat):
    model_type = float
    formatted_type = float
