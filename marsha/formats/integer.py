from .raw_format import RawFormat


class Integer(RawFormat):
    model_type = int
    formatted_type = int
