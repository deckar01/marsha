from .decimal import Decimal


class DecimalString(Decimal):
    formatted_type = str
