Home
####

A serialization and validation library for python 3 based on the typing module.

.. toctree::
   :hidden:

   self

.. toctree::
   :maxdepth: 2

   guide
   api
   genindex
