API Reference
=============

.. autosummary::
   :nosignatures:

   marsha.schema
   marsha.type
   marsha.load
   marsha.dump
   marsha.validate

.. automodule:: marsha
   :members: