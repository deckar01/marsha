from decimal import Decimal
import marsha


@marsha.schema()
class Book(dict):
    price: Decimal


def test_load():
    input = {'price': '42.00'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['price'] == Decimal('42.00')


def test_dump():
    data = Book(price=Decimal('42.00'))
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['price'] == '42.00'
