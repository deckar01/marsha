from typing import Callable
import marsha
from marsha.formats import IntegerString


@marsha.schema()
class PersonObject(marsha.Object):
    full_name: Callable
    first_name: str
    last_name: str

    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)


@marsha.schema()
class PersonDict(dict):
    full_name: Callable
    first_name: str
    last_name: str

    def full_name(self):
        return '{} {}'.format(self['first_name'], self['last_name'])


def test_dump_object():
    data = PersonObject(first_name='Jerry', last_name='Smith')
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['first_name'] == 'Jerry'
    assert output['last_name'] == 'Smith'
    assert output['full_name'] == 'Jerry Smith'


def test_dump_dict():
    data = PersonDict(first_name='Jerry', last_name='Smith')
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['first_name'] == 'Jerry'
    assert output['last_name'] == 'Smith'
    assert output['full_name'] == 'Jerry Smith'


def test_dump_type():
    integer_string = marsha.type(format=IntegerString)

    @marsha.schema()
    class Player(dict):
        points: integer_string
        penalties: integer_string
        total: Callable[..., integer_string]

        def total(self):
            return self['points'] - self['penalties']

    data = Player(points=25, penalties=3)
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['points'] == '25'
    assert output['penalties'] == '3'
    assert output['total'] == '22'


def test_dump_map():
    @marsha.schema()
    class Person(PersonDict):
        full_name: marsha.type(Callable, map='name')
    data = Person(first_name='Jerry', last_name='Smith')
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['first_name'] == 'Jerry'
    assert output['last_name'] == 'Smith'
    assert 'full_name' not in output
    assert output['name'] == 'Jerry Smith'


def test_load_object():
    input = {'first_name': 'Jerry', 'last_name': 'Smith'}
    output = marsha.load(input, PersonObject)
    assert output.__class__ == PersonObject
    assert output.first_name == 'Jerry'
    assert output.last_name == 'Smith'
    assert output.full_name.__func__ == PersonObject.full_name


def test_load_dict():
    input = {'first_name': 'Jerry', 'last_name': 'Smith'}
    output = marsha.load(input, PersonDict)
    assert output.__class__ == PersonDict
    assert output['first_name'] == 'Jerry'
    assert output['last_name'] == 'Smith'
    assert 'full_name' not in output
    assert output.full_name.__func__ == PersonDict.full_name
