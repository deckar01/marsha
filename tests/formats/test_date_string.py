from datetime import datetime
import pytest
import marsha


date = marsha.type(datetime, encoding='%m/%d/%Y')


@marsha.schema()
class Book(dict):
    published_date: date


def test_load():
    input = {'published_date': '1/2/2003'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['published_date'] == datetime(2003, 1, 2)


def test_load_wrong_type():
    input = {'published_date': 123}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'published_date': 'Expected str, but found int.'
    }


def test_load_wrong_encoding():
    input = {'published_date': '1-2-2013'}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'published_date':
            'Expected the encoding to be %m/%d/%Y, but found 1-2-2013.'
    }


def test_dump():
    data = Book(published_date=datetime(2003, 1, 2))
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['published_date'] == '01/02/2003'
