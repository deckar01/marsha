import pytest
import marsha


@marsha.schema()
class Book(dict):
    title: str


def test_load():
    input = {'title': 'Interlinked'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['title'] == 'Interlinked'


def test_min():
    min_str = marsha.type(str, min=1)

    @marsha.schema()
    class Book(dict):
        title: min_str
    input = {'title': 'Interlinked'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['title'] == 'Interlinked'


def test_fail_min():
    min_str = marsha.type(str, min=1)

    @marsha.schema()
    class Book(dict):
        title: min_str
    input = {'title': ''}
    with pytest.raises(ValueError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'title': 'Expected a minimum length of 1, but found 0.',
    }


def test_max():
    min_str = marsha.type(str, max=12)

    @marsha.schema()
    class Book(dict):
        title: min_str
    input = {'title': 'Interlinked'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['title'] == 'Interlinked'


def test_fail_max():
    min_str = marsha.type(str, max=12)

    @marsha.schema()
    class Book(dict):
        title: min_str
    input = {'title': 'A Brief History of Nearly Everything'}
    with pytest.raises(ValueError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'title': 'Expected a maximum length of 12, but found 36.',
    }


def test_dump():
    data = Book(title='Interlinked')
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['title'] == 'Interlinked'
