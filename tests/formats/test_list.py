from typing import List
import pytest
import marsha


@marsha.schema()
class Team(dict):
    points: List[int]


def test_load():
    input = {'points': [2, 3, 3, 1, 2]}
    output = marsha.load(input, Team)
    assert output.__class__ == Team
    assert isinstance(output['points'], list)
    assert output['points'] == [2, 3, 3, 1, 2]


def test_load_wrong_type():
    input = {'points': 23}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Team)
    assert exec_info.value.args[0] == {
        'points': 'Expected list, but found int.',
    }


def test_load_wrong_item_type():
    input = {'points': [2, '3', 3, 1.2, 2]}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Team)
    assert exec_info.value.args[0] == {
        'points': {
            1: 'Expected int, but found str.',
            3: 'Expected int, but found float.',
        }
    }


def test_load_nested_list():
    @marsha.schema()
    class Person(dict):
        name: str

    @marsha.schema()
    class Team(dict):
        members: List[Person]
    input = {'members': [{'name': 'Jerry'}]}
    output = marsha.load(input, Team)
    assert output.__class__ == Team
    assert isinstance(output['members'], list)
    assert len(output['members']) == 1
    assert output['members'][0]['name'] == 'Jerry'


def test_dump():
    data = Team(points=[2, 3, 3, 1, 2])
    output = marsha.dump(data, Team)
    assert output.__class__ == dict
    assert isinstance(output['points'], list)
    assert output['points'] == [2, 3, 3, 1, 2]
