from typing import Any
import marsha


@marsha.schema()
class Book(dict):
    meta: Any


class Unknown:
    pass


def test_load():
    input = {'meta': Unknown()}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['meta'].__class__ == Unknown


def test_dump():
    data = Book(meta=Unknown())
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['meta'].__class__ == Unknown
