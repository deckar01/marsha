import marsha


@marsha.schema()
class Person:
    pass


def test_load():
    input = {}
    output = marsha.load(input, Person)
    assert output.__class__ == Person


def test_dump():
    data = Person()
    output = marsha.dump(data)
    assert output.__class__ == dict


def test_load_nested():
    @marsha.schema()
    class Person(dict):
        name: str

    @marsha.schema()
    class Team(dict):
        leader: Person
    input = {'leader': {'name': 'Jerry'}}
    output = marsha.load(input, Team)
    assert output.__class__ == Team
    assert output['leader'].__class__ == Person
    assert output['leader']['name'] == 'Jerry'
