from typing import Union
from decimal import Decimal
import pytest
import marsha


@marsha.schema()
class Book(dict):
    volume: Union[int, Decimal]


def test_load_first_type():
    input = {'volume': 1}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['volume'] == 1


def test_load_second_type():
    input = {'volume': '2.1'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['volume'] == Decimal('2.1')


def test_load_wrong_type():
    input = {'volume': 2.1}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'volume': 'Expected int or Decimal, but found float.'
    }


def test_dump_first_type():
    data = Book(volume=1)
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['volume'] == 1


def test_dump_second_type():
    data = Book(volume=Decimal('2.1'))
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['volume'] == '2.1'


def test_dump_wrong_type():
    data = Book(volume=2.1)
    with pytest.raises(TypeError) as exec_info:
        marsha.dump(data)
    expected_message = 'Expected int or Decimal, but found float.'
    assert exec_info.value.args[0] == expected_message
