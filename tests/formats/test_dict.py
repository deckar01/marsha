import marsha


@marsha.schema()
class Person(dict):
    pass


def test_load():
    input = {}
    output = marsha.load(input, Person)
    assert output.__class__ == Person


def test_dump():
    data = Person()
    output = marsha.dump(data)
    assert output.__class__ == dict
