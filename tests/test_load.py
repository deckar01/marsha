from typing import List
from decimal import Decimal
import pytest
import marsha


@marsha.schema()
class Person(dict):
    name: str


def test_value():
    input = {'name': 'Jerry'}
    output = marsha.load(input, Person)
    assert output.__class__ == Person
    assert output['name'] == 'Jerry'


def test_list():
    input = [{'name': 'Jerry'}]
    output = marsha.load(input, List[Person])
    assert output.__class__ == list
    assert len(output) == 1
    assert output[0].__class__ == Person
    assert output[0]['name'] == 'Jerry'


def test_missing_key():
    input = {}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Person)
    assert exec_info.value.args[0] == {
        'name': 'This key was not found, but it is required.',
    }


def test_wrong_type():
    input = 'junk'
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Person)
    assert exec_info.value.args[0] == 'Expected dict, but found str.'


def test_wrong_value_type():
    input = {'name': 42}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Person)
    assert exec_info.value.args[0] == {
        'name': 'Expected str, but found int.',
    }


def test_default():
    @marsha.schema()
    class Person(dict):
        name: str = 'Rick'
    input = {}
    output = marsha.load(input, Person)
    assert output.__class__ == Person
    assert output['name'] == 'Rick'


def test_complex_type():
    @marsha.schema()
    class Book(dict):
        price: Decimal
    input = {'price': '42.00'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['price'] == Decimal('42.00')


def test_wrong_complex_type():
    @marsha.schema()
    class Book(dict):
        price: Decimal
    input = {'price': 42.00}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'price': 'Expected str, but found float.',
    }


def test_complex_default():
    @marsha.schema()
    class Book(dict):
        price: Decimal = Decimal('42.00')
    input = {}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['price'] == Decimal('42.00')
    print(id(Book.__marsha_format__))


def test_wrong_default_type():
    @marsha.schema()
    class Book(dict):
        price: Decimal = None
    input = {}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['price'] is None
