import pytest
import marsha


def test_register_dict():
    @marsha.schema()
    class Person(dict):
        pass
    format = marsha.FormatRegistry.get(Person)
    assert issubclass(format, marsha.formats.Dict)


def test_register_object():
    @marsha.schema()
    class Person:
        pass
    format = marsha.FormatRegistry.get(Person)
    assert issubclass(format, marsha.formats.Object)
    assert not issubclass(format, marsha.formats.Dict)


def test_unknown_type():
    class Thing:
        pass
    with pytest.raises(TypeError) as exec_info:
        @marsha.schema()
        class Person(dict):
            watch: Thing
    assert exec_info.value.args[0] == 'There is no default format for Thing.'


def test_reject_extra():
    @marsha.schema()
    class Person(dict):
        pass
    input = {'extra': 'junk'}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Person)
    assert exec_info.value.args[0] == {
        'extra': 'This key is not expected.',
    }


def test_ignore_extra():
    @marsha.schema(ignore_extra=True)
    class Person(dict):
        pass
    input = {'extra': 'junk'}
    output = marsha.load(input, Person)
    assert output == {}


def test_keep_extra():
    @marsha.schema(keep_extra=True)
    class Person(dict):
        pass
    input = {'extra': 'junk'}
    output = marsha.load(input, Person)
    assert output == {'extra': 'junk'}


def test_disable_ignore_extra():
    @marsha.schema(ignore_extra=False)
    class Person(dict):
        pass
    input = {'extra': 'junk'}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Person)
    assert exec_info.value.args[0] == {
        'extra': 'This key is not expected.',
    }


def test_disable_keep_extra():
    @marsha.schema(keep_extra=False)
    class Person(dict):
        pass
    input = {'extra': 'junk'}
    with pytest.raises(TypeError) as exec_info:
        marsha.load(input, Person)
    assert exec_info.value.args[0] == {
        'extra': 'This key is not expected.',
    }
