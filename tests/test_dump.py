from typing import List
import marsha


def test_dict():
    @marsha.schema()
    class Person(dict):
        name: str
    data = Person(name='Summer')
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['name'] == 'Summer'


def test_object():
    @marsha.schema()
    class Person(marsha.Object):
        name: str
    data = Person(name='Summer')
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['name'] == 'Summer'


def test_list():
    @marsha.schema()
    class Person(dict):
        name: str
    data = [Person(name='Summer')]
    output = marsha.dump(data, List[Person])
    assert output.__class__ == list
    assert len(output) == 1
    assert output[0].__class__ == dict
    assert output[0]['name'] == 'Summer'
