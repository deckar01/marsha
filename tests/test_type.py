import pytest
import marsha


custom_string = marsha.type(str)


@marsha.schema()
class Book(dict):
    title: custom_string


@marsha.schema()
class Person(dict):
    full_name: marsha.type(str, map='name')
    favorite_color: str


def test_load():
    input = {'title': 'Interlinked'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['title'] == 'Interlinked'


def test_dump():
    data = Book(title='Interlinked')
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['title'] == 'Interlinked'


def test_wrong_constraint():
    with pytest.raises(AttributeError) as exec_info:
        marsha.type(str, junk=True)
    expected_error = 'The String format has no constraint named junk.'
    assert exec_info.value.args[0] == expected_error


def test_load_map():
    input = {'name': 'Jerry', 'favorite_color': 'blue'}
    output = marsha.load(input, Person)
    assert output.__class__ == Person
    assert output['full_name'] == 'Jerry'
    assert output['favorite_color'] == 'blue'


def test_dump_map():
    data = Person(full_name='Jerry', favorite_color='blue')
    output = marsha.dump(data)
    assert output.__class__ == dict
    assert output['name'] == 'Jerry'
    assert output['favorite_color'] == 'blue'
