import pytest
import marsha
from decimal import Decimal


def test_dict_model_constraint():
    @marsha.schema()
    class Book(dict):
        price: Decimal

    @marsha.validate(Book)
    def price_must_be_positive(data):
        if data['price'] <= 0:
            message = 'Expected a positive price, but got {}.'
            return {'price': message.format(data['price'])}
    input = {'price': '42.00'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['price'] == Decimal('42.00')


def test_object_model_constraint():
    @marsha.schema()
    class Book(marsha.Object):
        price: Decimal

    @marsha.validate(Book)
    def price_must_be_positive(data):
        if data.price <= 0:
            message = 'Expected a positive price, but got {}.'
            return {'price': message.format(data.price)}
    input = {'price': '42.00'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output.price == Decimal('42.00')


def test_type():
    positive_decimal = marsha.type(Decimal)

    @marsha.validate(positive_decimal)
    def must_be_positive(value):
        if value <= 0:
            message = 'Expected a positive value, but got {}.'
            return message.format(value)

    @marsha.schema()
    class Book(dict):
        price: positive_decimal
    input = {'price': '42.00'}
    output = marsha.load(input, Book)
    assert output.__class__ == Book
    assert output['price'] == Decimal('42.00')


def test_failed_model_constraint():
    @marsha.schema()
    class Book(dict):
        price: Decimal

    @marsha.validate(Book)
    def price_must_be_positive(data):
        if data['price'] <= 0:
            message = 'Expected a positive price, but got {}.'
            return {'price': message.format(data['price'])}
    input = {'price': '-42.00'}
    with pytest.raises(ValueError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'price': 'Expected a positive price, but got -42.00.',
    }


def test_failed_nested_model_constraint():
    @marsha.schema()
    class Person(dict):
        name: str

    @marsha.schema()
    class Book(dict):
        author: Person

    @marsha.validate(Person)
    def a_person_must_have_a_name(data):
        if not data['name']:
            return {'name': 'Expected a non-empty string.'}
    input = {'author': {'name': ''}}
    with pytest.raises(ValueError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'author': {'name': 'Expected a non-empty string.'},
    }


def test_dict_failed_type_constraint():
    positive_decimal = marsha.type(Decimal)

    @marsha.validate(positive_decimal)
    def must_be_positive(value):
        if value <= 0:
            message = 'Expected a positive value, but got {}.'
            return message.format(value)

    @marsha.schema()
    class Book(dict):
        price: positive_decimal
    input = {'price': '-42.00'}
    with pytest.raises(ValueError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'price': 'Expected a positive value, but got -42.00.',
    }


def test_object_failed_type_constraint():
    positive_decimal = marsha.type(Decimal)

    @marsha.validate(positive_decimal)
    def must_be_positive(value):
        if value <= 0:
            message = 'Expected a positive value, but got {}.'
            return message.format(value)

    @marsha.schema()
    class Book(marsha.Object):
        price: positive_decimal
    input = {'price': '-42.00'}
    with pytest.raises(ValueError) as exec_info:
        marsha.load(input, Book)
    assert exec_info.value.args[0] == {
        'price': 'Expected a positive value, but got -42.00.',
    }
